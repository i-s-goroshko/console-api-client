﻿using System.Net;
using Newtonsoft.Json;

namespace ConsoleApp1.RestClients
{
    public class ApiResult
    {
        public HttpStatusCode StatusCode { get; set; }

        public bool IsSuccess { get; set; }

        public string ResponseJson { get; set; }

        public string ErrorMessage { get; private set; }

        public ApiResult(HttpStatusCode statusCode, bool isSuccess, string responseJson)
        {
            StatusCode = statusCode;
            IsSuccess = isSuccess;
            ResponseJson = responseJson;
        }

        public T As<T>()
        {
            return JsonConvert.DeserializeObject<T>(ResponseJson);
        }
    }
}
