﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ConsoleApp1.RestClients.Impl
{
    public class RestClientBase : HttpClient
    {
        private const string JsonContentType = "application/json";

        private const string _baseAddress = "";

        public string _basePath = "";

        public RestClientBase()
        {
            // configure content type
            DefaultRequestHeaders.Accept.Clear();
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonContentType));
        }

        public ApiResult Get(string path, object urlParams = null)
        {
            return SendRequest(path, HttpMethod.Get, null, urlParams);
        }

        public ApiResult Post(string path, object body = null, object urlParams = null)
        {
            return SendRequest(path, HttpMethod.Post, body, urlParams);
        }

        public ApiResult Put(string path, object body = null, object urlParams = null)
        {
            return SendRequest(path, HttpMethod.Put, body, urlParams);
        }

        public ApiResult Delete(string path, object urlParams = null)
        {
            return SendRequest(path, HttpMethod.Delete, null, urlParams);
        }

        //public ApiResult PostFast(string path, object body = null)
        //{
        //    // return SendRequest(path, HttpMethod.Post, body, urlParams);
        //    var request = new HttpRequestMessage(method, uriBuilder.Uri);
        //}

        //public async Task<IEnumerable<UserDto>> GetUsersInParallel(IEnumerable<int> userIds)
        //{
        //    var tasks = userIds.Select(id => client.GetUser(id));
        //    var users = await Task.WhenAll(tasks);

        //    return users;
        //}

        //public async Task<IEnumerable<UserDto>> GetUsersInParallel(IEnumerable<int> userIds)
        //{
        //    var tasks = userIds.Select(id => client.GetUser(id));
        //    var users = await Task.WhenAll(tasks);

        //    return users;
        //}

        private ApiResult SendRequest(string path, HttpMethod method, object body = null, object urlParams = null)
        {
            var uriBuilder = new UriBuilder(_baseAddress + _basePath + path);
            if (urlParams != null)
            {
                // convert dictionary to query params
                uriBuilder.Query = GetQueryString(urlParams);
            }

            var request = new HttpRequestMessage(method, uriBuilder.Uri);

            try
            {
                if (body != null)
                {
                    string json = JsonConvert.SerializeObject(body);
                    request.Content = new StringContent(json, Encoding.UTF8, JsonContentType);
                }

                using (var response = SendAsync(request, HttpCompletionOption.ResponseContentRead).Result)
                {
                    var responseContent = response.Content.ReadAsStringAsync().Result;
                    var apiResult = new ApiResult(response.StatusCode, response.IsSuccessStatusCode, responseContent);

                    //if (!response.IsSuccessStatusCode && response.StatusCode != System.Net.HttpStatusCode.BadRequest && path != "/login")
                    //{
                    //    throw new RestApiException(apiResult);
                    //}
                    return apiResult;
                }
            }
            finally
            {
                if (request.Content != null)
                {
                    request.Content.Dispose();
                }
            }
        }

        public string GetQueryString(object obj)
        {
            var result = new List<string>();
            var props = obj.GetType().GetProperties().Where(p => p.GetValue(obj, null) != null);
            foreach (var p in props)
            {
                var value = p.GetValue(obj, null);
                if (value is ICollection enumerable)
                {
                    result.AddRange(from object v in enumerable select string.Format("{0}={1}", p.Name, HttpUtility.UrlEncode(v.ToString())));
                }
                else
                {
                    result.Add(string.Format("{0}={1}", p.Name, HttpUtility.UrlEncode(value.ToString())));
                }
            }

            return string.Join("&", result.ToArray());
        }
    }
}
